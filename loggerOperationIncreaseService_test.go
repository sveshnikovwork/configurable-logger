package configurable_logger

import (
	"reflect"
	"testing"

	"github.com/google/uuid"
)

// Тестирование корректности модификации операций
func Test_loggerOperationIncreaseService_IncreaseOperations_IncreaseLevel(t *testing.T) {
	operationUuid := uuid.New().String()

	type args struct {
		operation LogOperation
	}
	tests := []struct {
		name string
		args args
		want LogOperation
	}{
		{
			name: "Тестирование корректности изменения уровня операции",
			args: args{
				LogOperation{
					Uuid:  operationUuid,
					Level: 0,
				},
			},
			want: LogOperation{
				Uuid:  operationUuid,
				Level: 1,
			},
		},
		{
			name: "Тестирование корректности изменения уровня операции",
			args: args{
				LogOperation{
					Uuid:  operationUuid,
					Level: 10,
				},
			},
			want: LogOperation{
				Uuid:  operationUuid,
				Level: 11,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := loggerOperationIncreaseService{}
			got := l.IncreaseOperations([]LogOperation{tt.args.operation})

			isError := true
			for _, operation := range got {
				if reflect.DeepEqual(operation, tt.want) {
					isError = false
				}
			}

			if isError {
				t.Errorf("IncreaseOperations() not returns wanted operation: %v", tt.want)
			}
		})
	}
}

// Тестирование корректности добавления новой операции в пул
func Test_loggerOperationIncreaseService_IncreaseOperations_AddingNewOperation(t *testing.T) {
	type args struct {
		operations []LogOperation
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Тестирование корректности добавления новой операции в пул",
			args: args{},
			want: 1,
		},
		{
			name: "Тестирование корректности добавления новой операции в пул",
			args: args{
				[]LogOperation{
					{
						Uuid:  uuid.New().String(),
						Level: 10,
					},
				},
			},
			want: 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := loggerOperationIncreaseService{}
			if got := l.IncreaseOperations(tt.args.operations); tt.want != len(got) {
				t.Errorf("IncreaseOperations() quantity = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование корректности задания UUID операции и ее уровня при создании.
func Test_loggerOperationIncreaseService_IncreaseOperations_NewOperationUuidAndLevel(t *testing.T) {
	tests := []struct {
		name string
	}{
		{
			name: "Тестирование корректности задания UUID операции и ее уровня при создании.",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := loggerOperationIncreaseService{}
			got := l.IncreaseOperations([]LogOperation{})

			if len(got) == 0 {
				t.Errorf("IncreaseOperations() new operation is not created.")
			}

			operation := got[0]

			if operation.Level != 1 {
				t.Errorf("IncreaseOperations() new operation has incorrect level = %v, want 1.", operation.Level)
			}

			if len(operation.Uuid) == 0 {
				t.Errorf("IncreaseOperations() new operation has empty UUID.")
			}
		})
	}
}
