module bitbucket.org/sveshnikovwork/configurable-logger

go 1.14

require (
	bitbucket.org/sveshnikovwork/structure_dumper v1.1.0
	github.com/fatih/color v1.9.0
	github.com/google/uuid v1.1.1
	github.com/mattn/go-colorable v0.1.6 // indirect
	golang.org/x/sys v0.0.0-20200409092240-59c9f1ba88fa // indirect
)
