package configurable_logger

import (
	"fmt"
	"reflect"
	"testing"
	"time"
)

// Подставка для тестирования логирования в консоль
type fakeConsoleLogger struct {
	LastMessage string
}

// Фейковое логирование в консоль
func (f *fakeConsoleLogger) Log(format string, a ...interface{}) {
	f.LastMessage = fmt.Sprintf(format, a...)
}

// Тестирование записи сообщения в лог
func Test_consoleLoggerStorageDriver_Store(t *testing.T) {
	consoleLogger := &fakeConsoleLogger{}

	type fields struct {
		writer tConsoleLogWriter
		colors map[LogMessageType]tConsoleLogWriter
	}
	type args struct {
		row logEntity
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "Тестирование логирования сообщений без операций и без данных",
			fields: fields{
				writer: consoleLogger.Log,
				colors: nil,
			},
			args: args{
				row: logEntity{
					LogParameters: LogParameters{
						Code:       100,
						Message:    "Test message",
						Data:       nil,
						Operations: nil,
					},
					Type:        NOTICE,
					ServiceName: "TestService",
				},
			},
			want: time.Now().Format("2006-01-02 15:04:05 Z07:00") + ` ☘ TestService ▶ NOTICE ☸ Code: 100 ─ Test message.`,
		},
		{
			name: "Тестирование логирования сообщений с 2 операциями и без данных",
			fields: fields{
				writer: consoleLogger.Log,
				colors: nil,
			},
			args: args{
				row: logEntity{
					LogParameters: LogParameters{
						Code:    100,
						Message: "Test message",
						Data:    nil,
						Operations: []LogOperation{
							{
								Uuid:  "829405-Uidlk2jf-940482-0595k2",
								Level: 10,
							},
							{
								Uuid:  "5v4265-Uidlk2jf-940482-0595k2",
								Level: 20,
							},
						},
					},
					Type:        NOTICE,
					ServiceName: "TestService",
				},
			},
			want: time.Now().Format("2006-01-02 15:04:05 Z07:00") + ` ☘ TestService ▶ NOTICE ☸ Code: 100 ─ Test message.
	Operations:
		▶ №1 ☸ UUID: 829405-Uidlk2jf-940482-0595k2 ☸ Level: 10
		▶ №2 ☸ UUID: 5v4265-Uidlk2jf-940482-0595k2 ☸ Level: 20`,
		},
		{
			name: "Тестирование логирования сообщений с 2 операциями и набором данных",
			fields: fields{
				writer: consoleLogger.Log,
				colors: nil,
			},
			args: args{
				row: logEntity{
					LogParameters: LogParameters{
						Code:    100,
						Message: "Test message",
						Data: map[string]interface{}{
							"test":   1,
							"test-2": 2,
						},
						Operations: []LogOperation{
							{
								Uuid:  "829405-Uidlk2jf-940482-0595k2",
								Level: 10,
							},
							{
								Uuid:  "5v4265-Uidlk2jf-940482-0595k2",
								Level: 20,
							},
						},
					},
					Type:        NOTICE,
					ServiceName: "TestService",
				},
			},
			want: time.Now().Format("2006-01-02 15:04:05 Z07:00") + ` ☘ TestService ▶ NOTICE ☸ Code: 100 ─ Test message.
	Operations:
		▶ №1 ☸ UUID: 829405-Uidlk2jf-940482-0595k2 ☸ Level: 10
		▶ №2 ☸ UUID: 5v4265-Uidlk2jf-940482-0595k2 ☸ Level: 20
	Data:
		{
			"test": 1,
			"test-2": 2
		}`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := consoleLoggerStorageDriver{
				writer: tt.fields.writer,
				colors: tt.fields.colors,
			}

			c.Store(tt.args.row)
			if got := consoleLogger.LastMessage; !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Store() = %v, want %v", got, tt.want)
			}
		})
	}
}
