package configurable_logger

// Подставка для тестирования логера
type loggerStorageDriverMock struct {
	IsCalled bool
	LastRow  logEntity
}

// Сохранение записи лога в хранилище
func (l *loggerStorageDriverMock) Store(row logEntity) {
	l.IsCalled = true
	l.LastRow = row
}
