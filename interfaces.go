package configurable_logger

type LogMessageType string

const (
	NOTICE  LogMessageType = "NOTICE"
	INFO    LogMessageType = "INFO"
	DEBUG   LogMessageType = "DEBUG"
	WARNING LogMessageType = "WARNING"
	ERROR   LogMessageType = "ERROR"
)

// Сущность операции в логе
type LogOperation struct {
	Uuid  string
	Level uint16
}

// Параметры логирования
type LogParameters struct {
	Code       uint64
	Message    string
	Data       interface{}
	Operations []LogOperation
}

// Сущность записи лога
type logEntity struct {
	LogParameters
	Type        LogMessageType
	ServiceName string
}

// Интерфейс логгера
type LoggerInterface interface {
	// Логирование на уровне Notice
	Notice(params LogParameters)

	// Логирование на уровне Info
	Info(params LogParameters)

	// Логирование отладочной информации
	Debug(params LogParameters)

	// Логирование предупреждений
	Warning(params LogParameters)

	// Логирование ошибок
	Error(params LogParameters)
}

// Драйвер для сохранения записи лога в определенное хранилище
type LoggerStorageDriverInterface interface {
	// Сохранение записи лога в хранилище
	Store(row logEntity)
}

// Интерфейс сервиса для упрощенного управления операциями в логах
type LoggerOperationIncreaseServiceInterface interface {
	// Инкрементное увеличение уровня операций и создание новой
	IncreaseOperations([]LogOperation) []LogOperation
}

// Тип, описывающий фабрику сервиса модификации операций
type TLoggerOperationIncreaseService = func() LoggerOperationIncreaseServiceInterface

// Тип, описывающий фабрику логгеров
type TLoggerFactory = func(serviceName string) LoggerInterface

// Тип, описывающий генератор фабрики логгеров
type TLoggerFactoryGenerator = func(isDebug bool, drivers ...LoggerStorageDriverInterface) TLoggerFactory
