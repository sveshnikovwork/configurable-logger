package configurable_logger

import "github.com/google/uuid"

// Сервис для упрощенного управления операциями в логах
type loggerOperationIncreaseService struct{}

// Инкрементное увеличение уровня операций и создание новой
func (l loggerOperationIncreaseService) IncreaseOperations(operations []LogOperation) []LogOperation {
	var methodOperations []LogOperation
	operations = append(operations, LogOperation{
		Uuid:  uuid.New().String(),
		Level: 0,
	})

	for _, operation := range operations {
		methodOperations = append(methodOperations, LogOperation{
			Uuid:  operation.Uuid,
			Level: operation.Level + 1,
		})
	}

	return methodOperations
}

// Фабрика службы
func LoggerOperationIncreaseService() LoggerOperationIncreaseServiceInterface {
	return &loggerOperationIncreaseService{}
}
