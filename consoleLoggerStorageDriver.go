package configurable_logger

import (
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/fatih/color"
)

// Специальный тип, для описания функции системного логера для вывода в консоль.
type tConsoleLogWriter = func(format string, a ...interface{})

// Драйвер для логирования сообщений в консоль
type consoleLoggerStorageDriver struct {
	writer tConsoleLogWriter
	colors map[LogMessageType]tConsoleLogWriter
}

// Сохранение записи лога в хранилище
func (c consoleLoggerStorageDriver) Store(row logEntity) {
	writer := c.writer
	if value, ok := c.colors[row.Type]; ok {
		writer = value
	}

	operations := []string{}
	for i, operation := range row.Operations {
		operations = append(
			operations,
			fmt.Sprintf(
				"\t\t▶ №%v ☸ UUID: %v ☸ Level: %v",
				i+1,
				operation.Uuid,
				operation.Level,
			),
		)
	}

	rowData := ""
	data, err := json.MarshalIndent(row.Data, "\t\t", "\t")
	if nil == err {
		rowData = string(data)
	}

	operationsStr := ""
	if 0 != len(operations) {
		operationsStr = fmt.Sprintf(
			"\n\tOperations:\n%v",
			strings.Join(operations, "\n"),
		)
	}

	dataStr := ""
	if 0 != len(rowData) && "null" != rowData {
		dataStr = fmt.Sprintf("\n\tData:\n\t\t%v", rowData)
	}

	logMessage := fmt.Sprintf(
		"%v ☘ %v ▶ %v ☸ Code: %v ─ %v.%v%v",
		time.Now().Format("2006-01-02 15:04:05 Z07:00"),
		row.ServiceName,
		row.Type,
		row.Code,
		row.Message,
		operationsStr,
		dataStr,
	)

	writer(logMessage)
}

// Фабрика драйвера
func ConsoleLoggerStorageDriver() LoggerStorageDriverInterface {
	return &consoleLoggerStorageDriver{
		writer: func(format string, a ...interface{}) {
			fmt.Printf(format, a...)
		},
		colors: map[LogMessageType]tConsoleLogWriter{
			INFO:    color.Cyan,
			NOTICE:  color.Blue,
			DEBUG:   color.Magenta,
			WARNING: color.Yellow,
			ERROR:   color.Red,
		},
	}
}

// Фабрика драйвера без цветов
func ConsoleLoggerStorageDriverWithoutColors() LoggerStorageDriverInterface {
	return &consoleLoggerStorageDriver{
		writer: func(format string, a ...interface{}) {
			log.Println(fmt.Sprintf(format, a...))
		},
		colors: map[LogMessageType]tConsoleLogWriter{},
	}
}
