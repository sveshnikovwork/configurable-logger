package configurable_logger

import (
	"testing"

	"bitbucket.org/sveshnikovwork/structure_dumper"
)

// Тестирование вызова DataConverter в методе Error
func Test_logger_Error_DataConverterCall(t *testing.T) {
	type fields struct {
		serviceName    string
		storageDrivers []LoggerStorageDriverInterface
		isDebugEnabled bool
		dataConverter  structure_dumper.StructureConverterInterface
	}
	type args struct {
		params LogParameters
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Тестирование вызова DataConverter",
			fields: fields{
				"test",
				[]LoggerStorageDriverInterface{
					&loggerStorageDriverMock{IsCalled: false},
				},
				true,
				&structureConverterMock{IsCalled: false},
			},
			args: args{LogParameters{
				Code:       100,
				Message:    "Test",
				Data:       nil,
				Operations: nil,
			}},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := logger{
				serviceName:    tt.fields.serviceName,
				storageDrivers: tt.fields.storageDrivers,
				isDebugEnabled: tt.fields.isDebugEnabled,
				dataConverter:  tt.fields.dataConverter,
			}

			l.Error(tt.args.params)
			structureConverter, _ := tt.fields.dataConverter.(*structureConverterMock)

			if structureConverter.IsCalled != tt.want {
				t.Errorf("Error() DataConverter call status: %v, want %v", structureConverter.IsCalled, tt.want)
			}
		})
	}
}

// Тестирование вызова LoggerStorage в методе Error
func Test_logger_Error_LoggerStorageCall(t *testing.T) {
	type fields struct {
		serviceName    string
		storageDrivers []LoggerStorageDriverInterface
		isDebugEnabled bool
		dataConverter  structure_dumper.StructureConverterInterface
	}
	type args struct {
		params LogParameters
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Тестирование вызова LoggerStorage",
			fields: fields{
				"test",
				[]LoggerStorageDriverInterface{
					&loggerStorageDriverMock{IsCalled: false},
					&loggerStorageDriverMock{IsCalled: false},
				},
				true,
				&structureConverterMock{IsCalled: false},
			},
			args: args{LogParameters{
				Code:       100,
				Message:    "Test",
				Data:       nil,
				Operations: nil,
			}},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := logger{
				serviceName:    tt.fields.serviceName,
				storageDrivers: tt.fields.storageDrivers,
				isDebugEnabled: tt.fields.isDebugEnabled,
				dataConverter:  tt.fields.dataConverter,
			}

			l.Error(tt.args.params)
			for i, loggerStorage := range tt.fields.storageDrivers {
				loggerStorage, _ := loggerStorage.(*loggerStorageDriverMock)
				if loggerStorage.IsCalled != tt.want {
					t.Errorf("Error() LoggerStorage #%v call status: %v, want %v", i, loggerStorage.IsCalled, tt.want)
				}
			}
		})
	}
}

// Тестирование установки названия сервиса в судность записи лога в методе Error
func Test_logger_Error_ServiceNameSet(t *testing.T) {
	type fields struct {
		serviceName    string
		storageDrivers []LoggerStorageDriverInterface
		isDebugEnabled bool
		dataConverter  structure_dumper.StructureConverterInterface
	}
	type args struct {
		params LogParameters
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "Тестирование установки названия сервиса в судность записи лога",
			fields: fields{
				"test",
				[]LoggerStorageDriverInterface{
					&loggerStorageDriverMock{IsCalled: false},
				},
				true,
				&structureConverterMock{IsCalled: false},
			},
			args: args{LogParameters{
				Code:       100,
				Message:    "Test",
				Data:       nil,
				Operations: nil,
			}},
			want: "test",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := logger{
				serviceName:    tt.fields.serviceName,
				storageDrivers: tt.fields.storageDrivers,
				isDebugEnabled: tt.fields.isDebugEnabled,
				dataConverter:  tt.fields.dataConverter,
			}

			l.Error(tt.args.params)
			for i, loggerStorage := range tt.fields.storageDrivers {
				loggerStorage, _ := loggerStorage.(*loggerStorageDriverMock)
				if loggerStorage.LastRow.ServiceName != tt.want {
					t.Errorf("Error() LoggerStorage #%v service name: %v, want %v", i, loggerStorage.LastRow.ServiceName, tt.want)
				}
			}
		})
	}
}
