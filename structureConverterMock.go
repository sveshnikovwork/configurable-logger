package configurable_logger

// Подставка для тестирования логера
type structureConverterMock struct {
	IsCalled bool
}

// Конвертирует переданный объект данных в простую коллекцию значений или простое значение
func (s *structureConverterMock) Convert(obj interface{}) interface{} {
	s.IsCalled = true

	return obj
}
