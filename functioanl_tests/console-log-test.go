package main

import configurableLogger "bitbucket.org/sveshnikovwork/configurable-logger"

func main() {
	incrementService := configurableLogger.LoggerOperationIncreaseService()
	factory := configurableLogger.LoggerFactoryGenerator(true)
	logger := factory("Functional Test Service")

	baseOperations := incrementService.IncreaseOperations([]configurableLogger.LogOperation{})

	parametersWithAll := configurableLogger.LogParameters{
		Code:    100,
		Message: "Test msg",
		Data: map[string]interface{}{
			"test":   1,
			"test-2": 2,
		},
		Operations: incrementService.IncreaseOperations(baseOperations),
	}

	parametersWithData := configurableLogger.LogParameters{
		Code:    100,
		Message: "Test msg",
		Data: map[string]interface{}{
			"test":   1,
			"test-2": 2,
		},
		Operations: nil,
	}

	parametersWithOperations := configurableLogger.LogParameters{
		Code:       100,
		Message:    "Test msg",
		Data:       nil,
		Operations: incrementService.IncreaseOperations(baseOperations),
	}

	parametersWithoutAll := configurableLogger.LogParameters{
		Code:       100,
		Message:    "Test msg",
		Data:       nil,
		Operations: nil,
	}

	printAllLines(logger, parametersWithoutAll)
	printAllLines(logger, parametersWithOperations)
	printAllLines(logger, parametersWithData)
	printAllLines(logger, parametersWithAll)
}

func printAllLines(logger configurableLogger.LoggerInterface, parameters configurableLogger.LogParameters) {
	logger.Debug(parameters)
	logger.Info(parameters)
	logger.Notice(parameters)
	logger.Warning(parameters)
	logger.Error(parameters)
}
