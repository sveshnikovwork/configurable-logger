package configurable_logger

import (
	structureDumper "bitbucket.org/sveshnikovwork/structure_dumper"
)

// Генератор фабрики логгера
func LoggerFactoryGenerator(isDebug bool, drivers ...LoggerStorageDriverInterface) TLoggerFactory {
	if 0 == len(drivers) {
		drivers = append(drivers, ConsoleLoggerStorageDriver())
	}

	return func(serviceName string) LoggerInterface {
		return newLogger(
			serviceName,
			drivers,
			isDebug,
			structureDumper.NewStructureConverter(),
		)
	}
}
