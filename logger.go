package configurable_logger

import structureDumper "bitbucket.org/sveshnikovwork/structure_dumper"

// Сервис логирования
type logger struct {
	serviceName    string
	storageDrivers []LoggerStorageDriverInterface
	isDebugEnabled bool
	dataConverter  structureDumper.StructureConverterInterface
}

// Логирование на уровне Notice
func (l logger) Notice(params LogParameters) {
	l.sendLogMessageToDrivers(
		l.makeLogEntity(NOTICE, params),
	)
}

// Логирование на уровне Info
func (l logger) Info(params LogParameters) {
	l.sendLogMessageToDrivers(
		l.makeLogEntity(INFO, params),
	)
}

// Логирование отладочной информации
func (l logger) Debug(params LogParameters) {
	if false == l.isDebugEnabled {
		return
	}

	l.sendLogMessageToDrivers(
		l.makeLogEntity(DEBUG, params),
	)
}

// Логирование предупреждений
func (l logger) Warning(params LogParameters) {
	l.sendLogMessageToDrivers(
		l.makeLogEntity(WARNING, params),
	)
}

// Логирование ошибок
func (l logger) Error(params LogParameters) {
	l.sendLogMessageToDrivers(
		l.makeLogEntity(ERROR, params),
	)
}

// Генерация сущности записи лога
func (l logger) makeLogEntity(logType LogMessageType, params LogParameters) logEntity {
	params.Data = l.dataConverter.Convert(params.Data)

	return logEntity{
		LogParameters: params,
		Type:          logType,
		ServiceName:   l.serviceName,
	}
}

// Отправка сообщения в соответствующий драйвер
func (l logger) sendLogMessageToDrivers(log logEntity) {
	for _, driver := range l.storageDrivers {
		driver.Store(log)
	}
}

// Фабрика логера
func newLogger(
	serviceName string,
	storageDrivers []LoggerStorageDriverInterface,
	isDebugEnabled bool,
	dataConverter structureDumper.StructureConverterInterface,
) LoggerInterface {
	return &logger{
		serviceName:    serviceName,
		storageDrivers: storageDrivers,
		isDebugEnabled: isDebugEnabled,
		dataConverter:  dataConverter,
	}
}
